import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// import App from './App.css';
import { HashRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

import userReducer from "./store/reducers/user";
import taskReducer from './store/reducers/task';

import * as serviceWorker from "./serviceWorker";

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducer = combineReducers({
  user: userReducer,
  task: taskReducer
});

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

const app = (
    <Provider store={store}>
      <HashRouter>
        <App />
      </HashRouter>
    </Provider>
  );

ReactDOM.render(app, document.getElementById("root"));

serviceWorker.unregister();