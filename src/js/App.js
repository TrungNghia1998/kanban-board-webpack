import React, { Component, Suspense, lazy } from "react";
// import "./App.css";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import asyncComponent from './shared/asyncComponent';

import UserForm from "./components/UserForm";
// import ListTask from "./components/ListTask/ListTask";
// import FormTask from "./components/FormTask/FormTask";

// const asyncUserForm = asyncComponent(() => {
//   return import('./components/UserForm');
// });

const asyncListTask = asyncComponent(() => {
  return import('./components/ListTask/ListTask');
});

const asyncFormTask = asyncComponent(() => {
  return import('./components/FormTask/FormTask');
});


// const ListTask = lazy(() => import("./components/ListTask/ListTask"));
// const FormTask = lazy(() => import("./components/FormTask/FormTask"));

class App extends Component {
  render() {
    let routes = (
        <Suspense fallback={<div>Loading ...</div>}>
          <Switch>
            <Route path="/" exact component={UserForm} />
            <Route path="/tasks" component={asyncListTask} />
            <Route path="/task/:id" component={asyncFormTask} />
            <Redirect to="/" />
          </Switch>
        </Suspense>
    );

    return <div className="App">{routes}</div>;
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.task.tasks,
  };
};

export default connect(mapStateToProps)(App);
