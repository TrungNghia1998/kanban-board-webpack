import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

const initialState = {
  name: "",
  date: "",
  email: "",
  age: 1,
  gender: false,
  informationEditUser: null,
  informationUser: null,
  showModalEditUser: false,
};

const onChangeInformationUser = (state, action) => {
  return updateObject(state, {
    informationEditUser: {
      [action.event.target.name]: action.event.target.value,
    },
  });
};

const onClearForm = state => {
  return updateObject(state, {
    name: "",
    date: "",
    email: "",
    age: 1,
    gender: false,
  });
};

const onUpdateUser = (state, action) => {
  localStorage.setItem("InformationUser", JSON.stringify(action.data));
  return updateObject(state, {
    showModalEditUser: !state.showModalEditUser,
    informationEditUser: action.data,
    informationUser: action.data,
  });
};

const onRegisterHandler = (state, action) => {
  let inforUser = { ...state.informationUser };
  inforUser = action.user;
  inforUser = {
    ...inforUser,
    id: Math.random(),
  };
  console.log(inforUser);
  localStorage.setItem("InformationUser", JSON.stringify(inforUser));
  return updateObject(state, {
    informationUser: inforUser,
    email: inforUser.email
  });
};

const onToggleModalEditUser = (state, action) => {
  if (action.event) {
    action.event.preventDefault();
    console.log(123);
  }
  let informationUser = JSON.parse(localStorage.getItem("InformationUser"));
  console.log(state.showModalEditUser);
  return updateObject(state, {
    name: informationUser.name,
    gender: informationUser.gender,
    age: informationUser.age,
    email: informationUser.email,
    date: informationUser.date,
    showModalEditUser: !state.showModalEditUser,
  });
};

const onChange = (state, action) => {
  return updateObject(state, {
    [action.event.target.name]: action.event.target.value,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ON_CLEAR_FORM:
      return onClearForm(state, action);
    case actionTypes.ON_CHANGE_INFORMATION_USER:
      return onChangeInformationUser(state, action);
    case actionTypes.ON_UPDATE_USER:
      return onUpdateUser(state, action);
    case actionTypes.ON_REGISTER_HANDLER:
      return onRegisterHandler(state, action);
    case actionTypes.ON_TOGGLE_MODAL_EDIT_USER:
      return onToggleModalEditUser(state, action);
    case actionTypes.ON_CHANGE:
      return onChange(state, action);
    default:
      return state;
  }
};

export default reducer;
