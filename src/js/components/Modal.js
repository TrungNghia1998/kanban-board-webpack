import React, { Component } from "react";

import { connect } from "react-redux";

import * as actions from "../store/actions/index";

class Modal extends Component {
  
  onSubmit = (event) => {
    event.preventDefault();
    this.props.onAddTask(this.props);
    this.props.onCloseModal();
  }

  render() {
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.onToggleModal}
                className="close"
                data-dismiss="modal">
                &times;
              </button>
              <h4 className="modal-title">Tạo task</h4>
            </div>
            <div className="modal-body">
              <form onSubmit={(event) => this.onSubmit(event)}>
                <div className="form-group">
                  <label>Tên task</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Tên task"
                    name="name"
                    value={this.props.name}
                    onChange={this.props.onChangeTaskForm}
                  />
                </div>
                <div className="form-group">
                  <label>Mô tả</label>
                  <input
                    type="text"
                    className="form-control"
                    name="description"
                    placeholder="Mô tả"
                    value={this.props.description}
                    onChange={this.props.onChangeTaskForm}
                  />
                </div>
                <div className="form-group">
                  <label>Người tạo</label>
                  <input
                    type="text"
                    className="form-control"
                    readOnly
                    value={this.props.user}
                    placeholder="Người tạo"
                  />
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={this.props.onToggleModal}
                className="btn btn-default">
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

const mapStateToProps = state => {
  return {
    name: state.task.name,
    description: state.task.description,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onToggleModal: () => dispatch(actions.onToggleModal()),
    onChangeTaskForm: (event) => dispatch(actions.onChangeTaskForm(event)),
    onAddTask: (task) => dispatch(actions.onAddTask(task)),
    onCloseModal: () => dispatch(actions.onCloseModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
