import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";

import Modal from "../Modal";
import ModalSwap from "../ModalSwap";
import ModalEditUser from "../ModalEditUser";

import { connect } from "react-redux";

import * as actions from "../../store/actions/index";

class ListTask extends Component {
  render() {
    let user = localStorage.getItem("InformationUser");
    let data = user;
    let dataJson = JSON.parse(data);
    let error = null;

    if(!dataJson) {
      alert('Chưa đăng nhập!');
      this.props.history.push('/');
      return null;
    }

    let modal = null;
    if (this.props.toggleModal) {
      modal = <Modal user={dataJson.name} />;
    } else {
      modal = null;
    }

    let modalSwap = null;
    if (this.props.showModalSwap) {
      modalSwap = <ModalSwap />;
    } else {
      modalSwap = null;
    }

    let modalEditUser = null;
    if (this.props.showModalEditUser) {
      modalEditUser = <ModalEditUser user={dataJson} />;
    } else {
      modalEditUser = null;
    }

    // let { tasks } = this.props;
    let tasks = JSON.parse(localStorage.getItem("List Tasks"));
    if(!tasks)
      tasks = [];
    let tasksNew = [];
    let tasksProcess = [];
    let tasksResolved = [];
    if (undefined !== tasks && tasks.length !== null) {
      tasks.forEach(function(task) {
        if (task.position === 1) {
          tasksNew.push(task);
        }
        if (task.position === 2) {
          tasksProcess.push(task);
        }
        if (task.position === 3) {
          tasksResolved.push(task);
        }
      });
    }

    let elementTasksNew = null;
    let elementTasksProcess = null;
    let elementTasksResolved = null;

    if (tasksNew) {
      elementTasksNew = tasksNew.map((taskNew, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskNew.name}
              <Link
                to={"/task/" + taskNew.id} 
                className="btn btn-success">
                View Detail
              </Link>
              <button
                onClick={() => this.props.onToggleModalSwap(taskNew.id)}
                className="btn btn-warning">
                Status
              </button>
              <button
                onClick={() => this.props.onDeleteTask(taskNew.id)}
                className="btn btn-danger"
              >
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksNew = null;
    }

    if (tasksProcess) {
      elementTasksProcess = tasksProcess.map((taskProcess, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskProcess.name}
              <Link
                to={"/task/" + taskProcess.id} 
                className="btn btn-success">
                View Detail
              </Link>
              <button
                onClick={() => this.props.onToggleModalSwap(taskProcess.id)}
                className="btn btn-warning">
                Edit
              </button>
              <button
                onClick={() => this.props.onDeleteTask(taskProcess.id)}
                className="btn btn-danger">
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksProcess = null;
    }

    if (tasksResolved) {
      elementTasksResolved = tasksResolved.map((taskResolved, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskResolved.name}
              <Link
                to={"/task/" + taskResolved.id} 
                className="btn btn-success">
                View Detail
              </Link>
              <button
                onClick={() => this.props.onToggleModalSwap(taskResolved.id)}
                className="btn btn-warning">
                Status
              </button>
              <button
                onClick={() => this.props.onDeleteTask(taskResolved.id)}
                className="btn btn-danger"
              >
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksProcess = null;
    }

    return (
      <div>
        {error}
        {modal}
        {modalSwap}
        {modalEditUser}
        <div>
          <nav className="navbar navbar-inverse">
            <div className="container-fluid">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <a
                    href="/"
                    onClick={(event) => this.props.onToggleModalEditUser(event)}>
                      {dataJson.email}
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
        <div className="form-group">
          <div className="col-xs-12">
            <div className="col-xs-4">
              <div className="panel panel-primary">
                <div className="panel-heading">New</div>
                {elementTasksNew}
              </div>
            </div>

            <div className="col-xs-3">
              <div className="panel panel-primary">
                <div className="panel-heading">Process</div>
                {elementTasksProcess}
              </div>
            </div>

            <div className="col-xs-3">
              <div className="panel panel-primary">
                <div className="panel-heading">Resolved</div>
                {elementTasksResolved}
              </div>
            </div>

            <div className="col-xs-2">
              <button
                onClick={this.props.onToggleModal}
                className="btn btn-success">
                Thêm mới
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    name: state.user.name,
    date: state.user.date,
    email: state.user.email,
    toggleModal: state.task.toggleModal,
    position: state.task.position,
    tasks: state.task.tasks,
    showModalSwap: state.task.showModalSwap,
    showModalEditUser: state.user.showModalEditUser,
    informationUser: state.user.informationUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onToggleModal: () => dispatch(actions.onToggleModal()),
    onToggleModalSwap: id => dispatch(actions.onToggleModalSwap(id)),
    onDeleteTask: id => dispatch(actions.onDeleteTask(id)),
    onToggleModalEditUser: (event) => dispatch(actions.onToggleModalEditUser(event))
  };
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ListTask));
