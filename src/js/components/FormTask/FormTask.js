import React, { Component } from "react";

import { connect } from "react-redux";

import * as actions from "../../store/actions/index";

class FormTask extends Component {

  onSubmit = (event) => {
    event.preventDefault();
    this.props.history.push("/tasks");
  };

  render() {
    let tasks = localStorage.getItem("List Tasks");
    let tasksArray = JSON.parse(tasks);
    let id = parseInt(this.props.match.params.id);
    let taskSelected = tasksArray.find(x => parseInt(x.id) === id);
    let userCreated = JSON.parse(localStorage.getItem("InformationUser"));
    
    return (
      <div className="col-xs-8">
        <form onSubmit={this.onSubmit}>
          <legend>Task Detail</legend>
          <div className="form-group">
            <label>ID Task: {taskSelected.id} </label>    
          </div>
          <div className="form-group">
            <label>Task Name: {taskSelected.name} </label>
          </div>
          <div className="form-group">
            <label>Description: {taskSelected.description} </label>
          </div>
          <div className="form-group">
            <label>Người tạo: {userCreated.name}</label>
          </div>
          <button type="submit" className="btn btn-primary">
            Go back
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    name: state.task.name,
    description: state.task.description,
    tasks: state.task.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeTaskForm: (event) => dispatch(actions.onChangeTaskForm(event))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormTask);
