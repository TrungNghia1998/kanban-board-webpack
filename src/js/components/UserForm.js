import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";

import * as actions from "../store/actions/index";

class UserForm extends Component {

  onSubmit = event => {
    event.preventDefault();
    this.props.onRegisterHandler(this.props); //Truyền state ra ngoài App.js
    this.props.onClearForm();
    this.props.history.push("/tasks");
  };

  render() {
    return (
      <div className="col-xs-8">
        <form onSubmit={this.onSubmit}>
          <legend>Thong tin nguoi dung</legend>

          <div className="form-group">
            <label>Ho ten</label>
            <input
              type="text"
              name="name"
              className="form-control"
              value={this.props.name}
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Ngay sinh</label>
            <input
              type="date"
              name="date"
              value={this.props.date}
              className="form-control"
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              name="email"
              value={this.props.email}
              className="form-control"
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Tuoi</label>
            <input
              type="number"
              name="age"
              value={this.props.age}
              className="form-control"
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Gioi tinh</label>
            <select
              className="form-control"
              name="gender"
              value={this.props.gender}
              onChange={this.props.onChange}>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div>

          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    name: state.user.name,
    date: state.user.date,
    email: state.user.email,
    age: state.user.age,
    gender: state.user.gender,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onClearForm: () => dispatch(actions.onClearForm()),
    onChange: (event) => dispatch(actions.onChange(event)),
    onRegisterHandler: (user) => dispatch(actions.onRegisterHandler(user))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserForm));
