import React, { Component } from "react";

import { connect } from "react-redux";

import * as actions from "../store/actions/index";

class ModalEditUser extends Component {

  onSubmit = (event) => {
    event.preventDefault();
    let data = {
      name: this.props.name,
      date: this.props.date,
      email: this.props.email,
      age: this.props.age,
      gender: this.props.gender
    }
    this.props.onUpdateUser(data);
    this.props.onCloseModal();
  }

  render() {
    console.log(this.props);
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.onToggleModalEditUser}
                className="close"
                data-dismiss="modal">
                &times;
              </button>
              <h4 className="modal-title">Thông tin người dùng</h4>
            </div>
            <div className="modal-body">
              <form onSubmit={(event) => this.onSubmit(event)}>
                <div className="form-group">
                  <label>Họ tên</label>
                  <input
                    type="text"
                    name="name"
                    className="form-control"
                    value={this.props.name}
                    onChange={this.props.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Ngày sinh</label>
                  <input
                    type="date"
                    name="date"
                    value={this.props.date}
                    className="form-control"
                    onChange={this.props.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    value={this.props.email}
                    className="form-control"
                    onChange={this.props.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Tuổi</label>
                  <input
                    type="number"
                    name="age"
                    value={this.props.age}
                    className="form-control"
                    onChange={this.props.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Giới tính</label>
                  <select
                    className="form-control"
                    name="gender"
                    value={this.props.gender}
                    onChange={this.props.onChange}
                  >
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>

                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={this.props.onToggleModalEditUser}
                className="btn btn-default">
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

const mapStateToProps = state => {
  return {
    name: state.user.name,
    date: state.user.date,
    email: state.user.email,
    age: state.user.age,
    gender: state.user.gender,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChange: (event) => dispatch(actions.onChange(event)),
    onCloseModal: () => dispatch(actions.onCloseModal()),
    onUpdateUser: (event, data) => dispatch(actions.onUpdateUser(event, data)),
    onToggleModalEditUser: () => dispatch(actions.onToggleModalEditUser()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalEditUser);
