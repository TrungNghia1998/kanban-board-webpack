import React, { Component } from "react";

import { connect } from "react-redux";

import * as actions from "../store/actions/index";

class ModalSwap extends Component {

  onSubmit = event => {
    event.preventDefault();
    this.props.onUpdatePosition(this.props);
    this.props.onCloseModal();
  };

  render() {
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.onToggleModalSwap}
                className="close"
                data-dismiss="modal"
              >
                &times;
              </button>
              <h4 className="modal-title">Chuyển vị trí</h4>
            </div>
            <div className="modal-body">
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <select
                    className="form-control"
                    name="position"
                    value={this.props.position}
                    onChange={this.props.onChangeTaskForm}>
                    <option value="1">New</option>
                    <option value="2">In process</option>
                    <option value="3">Resolved</option>
                  </select>
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
                <button
                  type="button"
                  onClick={this.props.onToggleModalSwap}
                  className="btn btn-default">
                  Close
                </button>
              </form>
            </div>
            
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

const mapStateToProps = state => {
  return {
    position: state.task.position
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCloseModal: () => dispatch(actions.onCloseModal()),
    onUpdatePosition: (data) => dispatch(actions.onUpdatePosition(data)),
    onToggleModalSwap : (id) => dispatch(actions.onToggleModalSwap(id)),
    onChangeTaskForm: (event) => dispatch(actions.onChangeTaskForm(event))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalSwap);
